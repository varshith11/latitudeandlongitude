    getLatitudeLongitude: function(component) {
        var precision = 6;  // maximum numbers after decimal point
        var decPts = Math.pow(10,precision);

        if (component.get("v.latDegree") != null && component.get("v.latDegree") != undefined) {
            let lat = (parseFloat(component.get("v.latDegree")) + (parseFloat(component.get("v.latMinute")) /60) + (parseFloat(component.get("v.latSecond")) /3600));
            lat = Math.round(lat * decPts)/decPts;
            let dir = component.get("v.latHemisphere");
            console.log('---lat dec--'+lat);
            console.log('---dir--'+dir);
            if (dir == "S" || dir == 'W')
            {
                lat = lat * -1;
            }
            console.log('---lat dec--'+lat);
            component.set("v.latitude",lat);
        }
        if (component.get("v.longDegree") != null && component.get("v.longDegree") != undefined) {
            let long = (parseFloat(component.get("v.longDegree")) + (parseFloat(component.get("v.longMinute")) /60) + (parseFloat(component.get("v.longSecond")) /3600));
            long = Math.round(long * decPts)/decPts;
            let dir = component.get("v.longHemisphere");
            console.log('---dir--'+dir);
            if (dir == "S" || dir == 'W')
            {
                long = long * -1;
            }
            console.log('---long dec--'+long);
            component.set("v.longitude",long);
        }
    },